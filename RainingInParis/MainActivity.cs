﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;

namespace RainingInParis
{
    [Activity(Label = "RainingInParis", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {

        CheckBox a;
        CheckBox b;
        CheckBox c;
        TextView t;
        ImageView img;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            a = FindViewById<CheckBox>(Resource.Id.checkBox1);
            b = FindViewById<CheckBox>(Resource.Id.checkBox2);
            c = FindViewById<CheckBox>(Resource.Id.checkBox3);

            t = FindViewById<TextView>(Resource.Id.textView2);

            img = FindViewById<ImageView>(Resource.Id.imageView1);

            Button button = FindViewById<Button>(Resource.Id.myButton);

            button.Click += checkRain;


        }


        public void checkRain(object o, EventArgs e) {
            if (a.Checked == true && b.Checked == true) {
                t.Text = "Yes, it's raining!";
                img.SetImageResource(Resource.Drawable.umbrella);
            }
            else {
                t.Text = "Nope, not raining!";
                img.SetImageResource(Resource.Drawable.sun);
            }
        }

    }
}

